(function () {
    "use strict";

    window.videoPuzzle = {
        settings: {
            proxy: "http://server3.tezzt.nl:1332/api/proxy",
            vidHeight: 0,
            vidWidth: 0,
            tileWidth: 0,
            tileHeight: 0,
            vidID: "LZuiGS2V4Ac",
            rows: 3,
            cols: 3,
            lastTileName: "tile22"
        },

        run: function () {
            videoPuzzle.init();
            videoPuzzle.createVideoNode();
        },

        sequenced: [],

        init: function () {
            videoPuzzle.settings.theTiles = [];
            addEventListener("keydown", function (e) {
                videoPuzzle.filterKey(e);
            });
            videoPuzzle.puzzleSettings();
        },
        getFirst: function (array) {
            var n = array[0];
            array.shift();
            return n;
        },
        getRandomArray: function (length) {
            var tiles = [];
            for (var i = 0; i < length; i = i + 1)
                tiles.push(i);
            return videoPuzzle.shuffle(tiles);
        },
        shuffle: function (array) {
            var currentIndex = array.length, temporaryValue, randomIndex;

            // While there remain elements to shuffle...
            while (0 !== currentIndex) {

                // Pick a remaining element...
                randomIndex = Math.floor(Math.random() * currentIndex);
                currentIndex -= 1;

                // And swap it with the current element.
                temporaryValue = array[currentIndex];
                array[currentIndex] = array[randomIndex];
                array[randomIndex] = temporaryValue;
            }
            return array;
        },

        moveTile: function (source) {
            var clickedTile = -1;
            var emptyTile = videoPuzzle.emptyTileSeq;

            for (var i = 0; i < videoPuzzle.settings.theTiles.length; i = i + 1) {
                if (videoPuzzle.settings.theTiles[i].sequence == source) {
                    clickedTile = videoPuzzle.settings.theTiles[i].sequence;
                }
            }

            switch (source) {
                case (parseInt(videoPuzzle.emptyTileSeq) - parseInt(videoPuzzle.settings.rows)):
                    console.log("up");
                    break;
                case (parseInt(videoPuzzle.emptyTileSeq) + parseInt(videoPuzzle.settings.rows)):
                    console.log("down");
                    break;
                case (parseInt(videoPuzzle.emptyTileSeq) - 1):
                    for (var i = 0; i < videoPuzzle.settings.rows; i++)
                        if (source == i * videoPuzzle.settings.cols - 1)
                            return;
                    console.log("left");
                    break;
                case(parseInt(videoPuzzle.emptyTileSeq) + 1):
                    for (var i = 0; i < videoPuzzle.settings.rows; i++)
                        if (source == i * videoPuzzle.settings.cols)
                            return;
                    console.log("right");
                    break;
                default:
                    console.log("Not adjecent");
                    return;
                    break;
            }

            if (clickedTile < 0 || emptyTile < 0) {
                console.log("Something went wrong.");
                return;
            } else if (clickedTile == emptyTile)
                return;

            var temp = videoPuzzle.settings.theTiles[emptyTile].sequence;
            videoPuzzle.settings.theTiles[emptyTile].sequence = videoPuzzle.settings.theTiles[clickedTile].sequence;
            videoPuzzle.settings.theTiles[clickedTile].sequence = temp;
            videoPuzzle.sequenced = videoPuzzle.sortTiles(videoPuzzle.settings.theTiles);
        },

        videoControls: function () {
            var body = document.querySelector("body");
            var div = document.createElement("div");
            var video = document.querySelector("video");

            div.id = "controls";

            var playpause = document.createElement("input");
            playpause.type = "button";
            playpause.value = "Play / Pause";
            playpause.addEventListener("click", function () {
                if (video.paused)
                    video.play();
                else
                    video.pause();
            });
            div.appendChild(playpause);

            var volume = document.createElement("label");
            var volumeUp = document.createElement("input");
            var volumeDown = document.createElement("input");
            var vol = 50;
            video.volume = vol / 100;

            volume.innerHTML = "Volume: " + vol;

            volumeUp.type = "button";
            volumeUp.value = "+";
            volumeUp.addEventListener("click", function () {
                if (vol < 100)
                    vol = vol + 10;
                video.volume = vol / 100;
                volume.innerHTML = "Volume: " + vol;
            });

            volumeDown.type = "button";
            volumeDown.value = "-";
            volumeDown.addEventListener("click", function () {
                if (vol > 0)
                    vol = vol - 10;
                video.volume = vol / 100;
                volume.innerHTML = "Volume: " + vol;
            });

            div.appendChild(volume);
            div.appendChild(volumeUp);
            div.appendChild(volumeDown);
            body.appendChild(div);
        },

        puzzleSettings: function () {
            var _url = location.href;
            var baseURI;
            var params;
            if (_url.indexOf("?") >= 0) {
                var strparts = _url.split("?");
                baseURI = strparts[0];
                params = strparts[1];
            }
            else {
                baseURI = _url;
            }

            if (params != null) {
                var arr = params.split("&");
                for (var i = 0; i < arr.length; i++) {
                    var kvp = arr[i].split("=");
                    switch (kvp[0]) {
                        case "ID":
                            if (kvp[1] != null && kvp[1] != "")
                                videoPuzzle.settings.vidID = kvp[1];
                            break;
                        case "rows":
                            if (kvp[1] != null)
                                videoPuzzle.settings.rows = kvp[1];
                            break;
                        case "cols":
                            if (kvp[1] != null)
                                videoPuzzle.settings.cols = kvp[1];
                            break;
                        default:
                            console.log("invalid param");
                    }
                }
                videoPuzzle.settings.lastTileName = "tile" + (videoPuzzle.settings.rows - 1).toString() + (videoPuzzle.settings.cols - 1).toString();
            }

            var body = document.querySelector("body");
            var form = document.createElement("form");
            form.method = "GET";

            var videoID = document.createElement("input");
            videoID.type = "text";
            videoID.value = videoPuzzle.settings.vidID;

            var nRows = document.createElement("input");
            nRows.type = "number";
            nRows.min = 2;
            nRows.max = 6;
            nRows.step = 1;
            nRows.value = videoPuzzle.settings.rows;
            var nCols = document.createElement("input");
            nCols.type = "number";
            nCols.min = 2;
            nCols.max = 6;
            nCols.step = 1;
            nCols.value = videoPuzzle.settings.cols;

            var submit = document.createElement("input");
            submit.type = "button";
            submit.value = "submit";
            submit.addEventListener("click", function () {
                var param1 = ("ID=" + videoID.value);
                var param2 = ("rows=" + nRows.value);
                var param3 = ("cols=" + nCols.value);

                var get = baseURI + "?" + param1 + "&" + param2 + "&" + param3;
                window.location.replace(get);
            });

            form.appendChild(videoID);
            form.appendChild(nRows);
            form.appendChild(nCols);
            form.appendChild(submit);

            body.appendChild(form);
        },

        createTiles: function () {
            var tile, tilecontainer, tilebox, rowCounter, colCounter, tileName, tileWidth, tileHeight;
            console.log(document.querySelector("#video").offsetWidth + ", " + document.querySelector("#video").offsetHeight);
            videoPuzzle.settings.vidWidth = document.querySelector("#video").offsetWidth;
            videoPuzzle.settings.vidHeight = document.querySelector("#video").offsetHeight;
            var vHeight = videoPuzzle.settings.vidHeight;
            var vWidth = videoPuzzle.settings.vidWidth;

            videoPuzzle.settings.tileHeight = Math.floor(vHeight / videoPuzzle.settings.rows);
            videoPuzzle.settings.tileWidth = Math.floor(vWidth / videoPuzzle.settings.cols);

            tileWidth = videoPuzzle.settings.tileWidth;
            tileHeight = videoPuzzle.settings.tileHeight;

            tilecontainer = document.createElement("div");
            tilecontainer.className = "tileContainer";
            tilecontainer.style.width = vWidth + 10 + "px";
            tilecontainer.style.height = vHeight + 10 + "px";

            document.body.appendChild(tilecontainer);
            var tileCounter = 0;
            var rTiles = videoPuzzle.getRandomArray(videoPuzzle.settings.rows * videoPuzzle.settings.cols);
            for (rowCounter = 0; rowCounter < videoPuzzle.settings.rows; rowCounter++) {
                for (colCounter = 0; colCounter < videoPuzzle.settings.cols; colCounter++) {
                    tileName = "tile" + rowCounter + colCounter;
                    tilebox = document.createElement("canvas");
                    tilebox.id = "canvas" + tileCounter;
                    tilebox.width = tileWidth;
                    tilebox.height = tileHeight;
                    tilebox.className = "tile";
                    tilebox.addEventListener("click", videoPuzzle.onClick);
                    tilecontainer.appendChild(tilebox);

                    tile = {
                        name: tileName,
                        row: rowCounter,
                        col: colCounter,
                        sequence: videoPuzzle.getFirst(rTiles),
                        finalPos: tileCounter
                    };
                    videoPuzzle.settings.theTiles.push(tile);
                    tileCounter++;
                }
            }
            videoPuzzle.emptyTileSeq = videoPuzzle.settings.theTiles[tileCounter];
            videoPuzzle.sequenced = videoPuzzle.sortTiles(videoPuzzle.settings.theTiles);
            videoPuzzle.update();
        },
        findEmpty: function () {
            for (var i = 0; i < videoPuzzle.settings.theTiles.length; i = i + 1)
                if (videoPuzzle.settings.theTiles[i].name == videoPuzzle.settings.lastTileName) {
                    videoPuzzle.emptyTileSeq = videoPuzzle.settings.theTiles[i].sequence;
                }
        },
        onClick: function (e) {
            videoPuzzle.findEmpty();
            var clickedID = e.srcElement.id;
            clickedID = parseInt(clickedID.replace("canvas", ""));
            videoPuzzle.moveTile(clickedID);
        },

        filterKey: function (e) {
            videoPuzzle.findEmpty();
            var ret = -1;
            switch (e.which) {
                case (38):
                    console.log("up");
                    ret = parseInt(videoPuzzle.emptyTileSeq) - parseInt(videoPuzzle.settings.rows);
                    break;
                case (40):
                    console.log("down");
                    ret = parseInt(videoPuzzle.emptyTileSeq) + parseInt(videoPuzzle.settings.rows);
                    break;
                case (37):
                    console.log("left");
                    ret = parseInt(videoPuzzle.emptyTileSeq) - 1;
                    break;
                case (39):
                    console.log("right");
                    ret = parseInt(videoPuzzle.emptyTileSeq) + 1;
                    break;
                default:
                    console.log("This key is not used");
                    return;
                    break;
            }
            if (ret != -1)
                videoPuzzle.moveTile(ret);
        }
        ,

        createVideoNode: function () {
            var bodyNode = document.querySelector("body");
            var videoNode = document.createElement("video");
            var cb;
            videoNode.id = "video";
            videoNode.autoplay = "autoplay";
            videoNode.loop = "loop";
            bodyNode.appendChild(videoNode);
            videoNode.addEventListener("loadeddata", videoPuzzle.createTiles);

            var imgNode = document.createElement("img");
            imgNode.src = "images/1x1.png";
            imgNode.id = "img";
            imgNode.style.visibility = "hidden";
            bodyNode.appendChild(imgNode);

            cb = function (video) {
                console.log(video.title);
                var webm = video.getSource("video/webm", "medium");
                console.log("WebM " + webm.url);
                videoNode.src = webm.url;
            };
            YoutubeVideo(videoPuzzle.settings.vidID, videoPuzzle.settings.proxy, cb);
            videoPuzzle.videoControls();
        },

        sortTiles: function (myTiles) {
            var swapped;
            do {
                swapped = false;
                for (var i = 0; i < myTiles.length - 1; i = i + 1) {
                    if (myTiles[i].sequence > myTiles[i + 1].sequence) {
                        var temp = myTiles[i];
                        myTiles[i] = myTiles[i + 1];
                        myTiles[i + 1] = temp;
                        swapped = true;
                    }
                }
            } while (swapped);
            return myTiles;
        }
        ,
        update: function () {
            var tw = videoPuzzle.settings.tileWidth;
            var th = videoPuzzle.settings.tileHeight;
            var video = document.querySelector("#video");
            var img = document.querySelector("#img");
            var finished = true;
            var canvas;
            var context;

            var i = 0;
            for (var ri = 0; ri < videoPuzzle.settings.rows; ri++) {
                for (var ci = 0; ci < videoPuzzle.settings.cols; ci++) {
                    var tile = videoPuzzle.sequenced[i];
                    if (tile.sequence != tile.finalPos)
                        finished = false;
                    canvas = document.querySelector("#canvas" + i);
                    //ri.toString() + ci.toString());
                    context = canvas.getContext('2d');
                    if (tile.name == videoPuzzle.settings.lastTileName) {
                        context.drawImage(img, 0, 0, tw, th);
                    }
                    else {
                        context.drawImage(video, tile.col * tw, tile.row * th, tw, th, 0, 0, tw, th);
                    }
                    i++;
                }
            }
            if (finished)
                alert("Gewonnen!");
            else {
                setTimeout(function () {
                    videoPuzzle.update()
                }, 33);
            }
        }
    }
}());
window.addEventListener("load", videoPuzzle.run);